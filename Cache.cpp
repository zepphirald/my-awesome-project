#include "pch.h"
#include <unordered_map>
#include <string>
#include <iostream>

using namespace std;

// ���-����� � �������������� unordered_map � �������� ���� ������
class CacheAside {
private:
    unordered_map<string, string> cache;
    // ������� ��� ��������� ������ �� ���� ������
    string fetchDataFromDB(string key) {
        // ������ ��������� ������� � ���� ������ ���������� ������ ������-��������
        return "Data for " + key;
    }
public:
    // ������� ��������� ������ �� ����-������
    string get(string key) {
        // ��������� ������� ������ � ����
        if (cache.find(key) != cache.end()) {
            // ������ ���� � ����, ���������� ��
            cout << "Data found in cache: ";
            return cache[key];
        }
        // ������ ��� � ����, ����������� �� �� ���� ������
        cout << "Data not found in cache, fetching from DB: ";
        string data = fetchDataFromDB(key);
        // ��������� ������ � ����
        cache[key] = data;
        return data;
    }
};



// ���-����� write-through � �������������� unordered_map � �������� ���� ������
class WriteThroughCache {
private:
    unordered_map<string, string> cache;
    // ���� ������ ��� ����-������
    unordered_map<string, string>& database;
public:
    // ����������� ������, ������� ��������� ������ �� ���� ������
    WriteThroughCache(unordered_map<string, string>& db) : database(db) {}

    // ������� ��������� ������ �� ����-������
    string get(string key) {
        // ��������� ������� ������ � ����
        if (cache.find(key) != cache.end()) {
            // ������ ���� � ����, ���������� ��
            cout << "Data found in cache: ";
            return cache[key];
        }
        // ������ ��� � ����, ����������� �� �� ���� ������
        cout << "Data not found in cache, fetching from DB: ";
        string data = database[key];
        // ��������� ������ � ����
        cache[key] = data;
        return data;
    }
    // ������� ������ ������ � ���-����� � ���� ������
    void put(string key, string value) {
        // ���������� ������ � ���-����� � ���� ������
        cache[key] = value;
        database[key] = value;
        cout << "Data written to cache and database" << endl;
    }
};

int main() {
    CacheAside cache;
    cout << cache.get("key1") << endl;
    cout << cache.get("key2") << endl;
    cout << cache.get("key1") << endl;

    // ������� ������ ����-������ � �������������� unordered_map � �������� ���� ������
    unordered_map<string, string> db;
    WriteThroughCache cache2(db);

    // �������� ������ �� ����-������, ������� ��� � ���� ������
    cout << cache2.get("key1") << endl;

    // ���������� ������ � ���-����� � ���� ������
    cache2.put("key1", "value1");

    // �������� ������ �� ����-������, ������� ��� ���� � ���� ������
    cout << cache2.get("key1") << endl;
}
